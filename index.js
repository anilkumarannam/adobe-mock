const displayDropdownMenu = (firingElementId, targetElementId) => {
  const firingElement = document.getElementById(firingElementId);
  firingElement.addEventListener('click', () => {
    const targetElement = document.getElementById(targetElementId);
    targetElement.classList.toggle('hide');
  });

}

displayDropdownMenu('creativity-dropdown', 'creativity-dropdown-menu');